import {
	ConsoleLogger,
	DefaultDeviceController,
	DefaultMeetingSession,
	LogLevel,
	MeetingSessionConfiguration
} from 'amazon-chime-sdk-js';

const logger = new ConsoleLogger('MyLogger', LogLevel.INFO);
const deviceController = new DefaultDeviceController(logger);
// You must use "us-east-1" as the region for Chime API and set the endpoint.

var meetingSession = null;
var meetingAudio = null;
export async function joinMeeting(isPanelist, meetingData) {
	var joinInfo = (await getMeetingInfo(meetingData)).JoinInfo;
	var configuration = new MeetingSessionConfiguration(joinInfo.Meeting, joinInfo.Attendee);
	meetingSession = new DefaultMeetingSession(configuration, logger, deviceController);
	// In the usage examples below, you will use this meetingSession object.
	meetingSession = new DefaultMeetingSession(
		configuration,
		logger,
		deviceController
	);
	if (isPanelist) {
		await bindMeetingPanelistDevices();
	}
	startMeetingAudio();
	startMeetingVideo();

}

async function bindMeetingPanelistDevices() {
	var audioInputDevices = await meetingSession.audioVideo.listAudioInputDevices();
	var videoInputDevices = await meetingSession.audioVideo.listVideoInputDevices();
	console.log(audioInputDevices);
	console.log(videoInputDevices);
	var audioInputDeviceInfo = audioInputDevices[0];
	var videoInputDeviceInfo = videoInputDevices[0];
	if (audioInputDeviceInfo) {
		await meetingSession.audioVideo.chooseAudioInputDevice(audioInputDeviceInfo.deviceId);
	}
	if (videoInputDeviceInfo) {
		await meetingSession.audioVideo.chooseVideoInputDevice(videoInputDeviceInfo.deviceId);
	}
}

function startMeetingAudio() {
	meetingAudio = document.getElementById("meetingAudio");
	meetingSession.audioVideo.bindAudioElement(meetingAudio);
	meetingSession.audioVideo.start();
}


const videoElements = document.getElementsByClassName("panelist-video");

// index-tileId pairs
var indexMap = {};

const acquireVideoElement = tileId => {
	// Return the same video element if already bound.
	for (let i = 0; i < 16; i += 1) {
		if (indexMap[i] === tileId) {
			return videoElements[i];
		}
	}
	// Return the next available video element.
	for (let i = 0; i < 16; i += 1) {
		if (!Object.prototype.hasOwnProperty.call(indexMap, i)) {
			indexMap[i] = tileId;
			return videoElements[i];
		}
	}
	throw new Error('no video element is available');
};
async function getMeetingInfo(meetingData) {
	//UPDATE THIS URL 
	//const response = await fetch("https://chime-test.conveneagm.com/join?title="+meetingData.meetingName+"&name="+meetingData.name+"&region=us-east-1",{method: 'POST'});
	const response = await fetch("http://localhost:8080/join?meetingId=" + meetingData.meetingName + "&name=" + meetingData.name, { method: 'POST' })
	return response.json();
}
const releaseVideoElement = tileId => {
	for (let i = 0; i < 16; i += 1) {
		if (indexMap[i] === tileId) {
			delete indexMap[i];
			return;
		}
	}
};



function startMeetingVideo() {
	var observer = {
		// videoTileDidUpdate is called whenever a new tile is created or tileState changes.
		videoTileDidUpdate: tileState => {
			// Ignore a tile without attendee ID, a local tile (your video), and a content share.
			if (!tileState.boundAttendeeId || tileState.localTile || tileState.isContent) {
				return;
			}

			meetingSession.audioVideo.bindVideoElement(
				tileState.tileId,
				acquireVideoElement(tileState.tileId)
			);
		},
		videoTileWasRemoved: tileId => {
			releaseVideoElement(tileId);
		}
	};
	meetingSession.audioVideo.addObserver(observer);
	meetingSession.audioVideo.startLocalVideoTile();

}
